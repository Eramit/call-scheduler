import * as types from "./types";
import {
  getWeek,
  getStoredBookedSlots,
  updateSlotInStorage,
  getHours,
  validateSlots
} from "../utils";

export const initializeApp = dispatch => {
  const hours = getHours();
  const week = getWeek(hours);
  const bookedSlots = getStoredBookedSlots();
  const validSlots = validateSlots(bookedSlots);
  dispatch(setWeek(week));
  dispatch(setBookedSlots(validSlots));
};

export const bookCall = (day, slot, data, date) => (dispatch, getState) => {
  dispatch({
    type: types.BOOK_CALL,
    payload: { id: `${day}-${slot}`, data }
  });
  updateSlotInStorage(day, slot, data, date);
};

export const setWeek = week => dispatch => {
  dispatch({
    type: types.SET_WEEK,
    payload: week
  });
};

export const setBookedSlots = bookedSlots => dispatch => {
  dispatch({
    type: types.SET_BOOKED_SLOTS,
    payload: bookedSlots
  });
};
