import * as types from "./types";
const initialState = {
  week: [],
  bookedSlots: {}
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.BOOK_CALL:
      const { id, data } = action.payload;
      return {
        ...state,
        bookedSlots: { ...state.bookedSlots, [id]: data }
      };
    case types.SET_BOOKED_SLOTS:
      return {
        ...state,
        bookedSlots: action.payload
      };
    case types.SET_WEEK:
      return {
        ...state,
        week: action.payload
      };
    default:
      return state;
  }
};
