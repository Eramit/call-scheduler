export const addDays = (date, numberOfDaysToAdd) => {
  date.setDate(date.getDate() + numberOfDaysToAdd);
  return date;
};

const baseDays = [1, 2, 3, 4, 5, 6];
const currentDate = new Date();
const currentDay = currentDate.getDay();

const disableSlots = hours => hours.map( hour => ({
  ...hour,
  disabled: (currentDate.getHours() >= hour.slot)
}));

export const getWeek = hours => {
  return baseDays.map(day => ({
    value: day,
    disabled: currentDay - day > 0,
    date: addDays(new Date(), day - currentDay),
    hours: currentDay - day === 0 ? disableSlots(hours) : hours
  }));
};

export const getStoredBookedSlots = () => JSON.parse(localStorage.getItem("bookedSlots") || "{}");

export const updateSlotInStorage = (day, slot, data, date) => {
  const formatedDate = date.toISOString();
  
  const bookedSlots = getStoredBookedSlots();
  bookedSlots[`${day}-${slot}`] = {
    data,
    date: formatedDate
  };

  localStorage.setItem("bookedSlots", JSON.stringify(bookedSlots));
};

export const getHours = () => {
  const [start, end] = [8, 17];
  return Array.from({ length: end - start }, (_, i) => ({ slot: start + i }));
};

export const validateSlots = bookedSlots => {
  return Object.keys(bookedSlots).filter(slot => {
    const date = new Date(bookedSlots[slot].date);
    const firstDay = addDays(new Date(), baseDays[0] - currentDay);
    const lastDay = addDays(new Date(), baseDays[baseDays.length - 1] - currentDay);
    console.log(lastDay.getTime(), date.getTime(), firstDay.getTime());
    return date.getTime() <= lastDay.getTime() && date.getTime() >= firstDay.getTime();
  }).reduce((all, slot) => {
    return {...all, [slot]: bookedSlots[slot].data };
  }, {})
}



export function formatAMPM(hour) {
  let hours = hour;
  const ampm = hours >= 12 ? 'PM' : 'AM';
  hours = hours % 12;
  hours = hours ? hours : 12;
  return `${hours} ${ampm}`;
}


export const dayNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];