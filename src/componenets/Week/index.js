import React from "react";
import Day from "../Day";
import { useSelector } from "react-redux";
import "./index.scss";

export default function Week(props) {
  const days = useSelector(state => state.scheduler.week);
  return (
    <React.Fragment>
      <div className="Week">
        {days.map(day => (
          <Day key={day.value} day={day} className="WeekItem" />
        ))}
      </div>
    </React.Fragment>
  );
}
