import React from "react";
import { useDispatch } from "react-redux";
import { initializeApp } from "../../redux/actions"
import Week from "../Week";
import "./index.scss";

export default function App() {
  const dispatch = useDispatch();
  React.useEffect(() => {
    dispatch(initializeApp)
  }, [dispatch])
  return <main className="App">
    <h1> Call Scheduler </h1>
    <Week />
  </main>
}