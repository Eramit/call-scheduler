import React from "react";
import { formatAMPM } from "../../utils";
import "./index.scss";

export default function Modal({ isOpen, onSubmit, closeModal, modalData }) {
  return (
    <div className={`Modal ${isOpen ? "Modal--isOpen" : ""}`}>
      <form
        className="Modal-form"
        onSubmit={e => {
          e.preventDefault();
          const {
            name: { value: name },
            phone: { value: phone },
            email: { value: email }
          } = e.target;
          onSubmit({
            name,
            phone,
            email
          });
        }}
      >
        {modalData && (
          <div>
            Book slot for {modalData.date.toISOString().slice(0, 10)}{" "}
            {formatAMPM(modalData.slot)} to {formatAMPM(modalData.slot + 1)}{" "}
          </div>
        )}
        <input
          className="Modal-input"
          type="text"
          name="name"
          placeholder="Name"
          required
        ></input>
        <input
          className="Modal-input"
          type="tel"
          name="phone"
          placeholder="Phone"
          required
        ></input>
        <input
          className="Modal-input"
          type="email"
          name="email"
          placeholder="Email (Optional)"
        ></input>
        <div className="Modal-buttonContainer">
          <button type="submit" className="Modal-button Modal-button--success">
            Save
          </button>
          <button
            type="button"
            className="Modal-button Modal-button--warning"
            onClick={closeModal}
          >
            Cancel
          </button>
        </div>
      </form>
    </div>
  );
}
