import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { bookCall } from "../../redux/actions";
import Modal from "../Modal";
import { formatAMPM, dayNames } from "../../utils";
import "./index.scss";

export default function Day({ day, className }) {
  const [modalData, setModalData] = React.useState(null);
  const dispatch = useDispatch();
  const bookedSlots = useSelector(state => state.scheduler.bookedSlots);

  return (
    <div className={`${className} Day ${day.disabled ? "Day--disabled" : ""}`}>
      <div className="DayHeader">{dayNames[day.value]}</div>
      {day.hours.map(({ slot, disabled }) => {
        const data = bookedSlots[`${day.value}-${slot}`];
        return (
          <div
            key={slot}
            className={`DayBox ${data ? "DayBox--booked" : ""} ${
              disabled ? "DayBox--disabled" : ""
            }`}
            onClick={() => {
              setModalData({ day: day.value, slot, date: day.date });
            }}
          >
            <div className="DayBox-slot">{formatAMPM(slot)}</div>
            {data && (
              <div>
                Call with {data.name} ({data.phone})
                {data.email && <div className="DayBox-email">{data.email}</div>}
              </div>
            )}
          </div>
        );
      })}
      {!!modalData && (
        <Modal
          isOpen
          closeModal={() => setModalData(null)}
          modalData={modalData}
          onSubmit={data => {
            dispatch(
              bookCall(modalData.day, modalData.slot, data, modalData.date)
            );
            setModalData(null);
          }}
        />
      )}
    </div>
  );
}
